import re
from django_redis import get_redis_connection
from rest_framework import serializers
from .models import User


#接受　验证　创建
# 原有的封装方法密码没有加密不能使用ModelSerializer,多个参数并不存在模型类的属性中,注册逻辑只考虑反序列化器,不考虑序列化器
class UserRegisterSeializers(serializers.Serializer):

    usrename = serializers.CharField(
        max_length=20,
        min_length=5
    )
    def validated_data(self, value):
        if User.objects.filter(usernam=value).count() > 0:
            raise serializers.ValidationError("用户名已经存在")
        return value
    password = serializers.CharField(
        max_length=20,
        min_length=8
    )
    password2 = serializers.CharField()

    sms_code = serializers.ImageField()
    mobile = serializers.CharField()
    allow = serializers.BooleanField(write_only=True)
    def validate(self, attrs):
        passworld1 = attrs.get('passworld')
        passworld2 = attrs.get('passworld2')
        if passworld1 != passworld2:
            raise serializers.ValidationError("密码不一致")
        mobile = attrs.get("mobile")
        sms_code = attrs.get("sms_code")
        redis_cli = get_redis_connection("sms")
        sms_code_redis = redis_cli.get("sms_"+mobile)
        if sms_code_redis is None:
            raise serializers.ValidationError("短信验证码过期")
        if sms_code != int(sms_code_redis):
            raise serializers.ValidationError("短信验证错误")
        return attrs

    def validate_mobile(self, value):
        if not re.match(r"^1[3-9]\d{9}"):
            raise serializers.ValidationError("手机号格式错误")
        if User.objects.filter(mobile=value).count() > 0:
            raise serializers.ValidationError("手机号已经存在")
        return value

    def validate_alow(self, value):
        if not value:
            raise serializers.ValidationError("必须同意协议")
        return value

    def create(self,validated_data):
        #validata_data 满足验证的请求
        username = validated_data.get("user_name")
        password = validated_data.get("password")
        mobile = validated_data.get("mobile")
        #创建对象保存
        user = User()
        user.user_name = username
        # set_password从官方文档知道，对密码加密后在保存到属性
        user.set_password(password)
        user.mobile = mobile
        user.save()

        return {
            'id': user.id,
            'username': username,
            'mobile': mobile
        }


      












