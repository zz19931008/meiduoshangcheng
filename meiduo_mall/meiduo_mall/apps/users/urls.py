from django.conf.urls import url
from . import views

urlpatterns = [
    url('^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    url('^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url('^user/$',views.RegisterView.as_view())
]