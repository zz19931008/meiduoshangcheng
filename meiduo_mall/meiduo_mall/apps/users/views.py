from rest_framework.response import Response
from rest_framework.views import APIView
from .models import User
from rest_framework import generics
from . import serializers

class UsernameCountView(APIView):
    # 查询用户名是否存在
    def get(self, request, username):
        count = User.objects.filer(username=username).count()
        return Response({
            "username": "username",
            'count': count
        })


class MobileCountView(APIView):

    def get(self,request,mobile):
        count = User.objects.filer(mobile=mobile).count()
        return Response({
            "mobile": mobile,
            "count": count,
        })


class RegisterView(generics.CreateAPIView):
    """
    注册，创建新用户对象
    """
    # 指定查询集　创建此处不需要制定查询集
    # 指定序列化器,验证接受的数据和创建
    serializer_class = serializers.UserRegisterSeializers