from rest_framework.views import APIView
from rest_framework.response import Response
from django_redis import get_redis_connection
import random
from verifications import constants
from celery_tasks.sms.tasks  import send_sms

class SmsAPIView(APIView):
    def get(self,request,mobile):
        """
        向ｒｅｄｉｓ中保存数据，包含有效时间保存
        １验证６０内是否向此手机号发送短信，如果发过则返回
        ２如果未改过则继续执行
        ３生成随机六位验证码
        ４保存验证码，发送标记
        ４发送
        """
        redis_cli = get_redis_connection("sms_code")
        if redis_cli.get("sms_flag_"+mobile):
            return Response({'message': "请稍后再发"})
        sms_code = random.randint(100000, 999999)
        redis_pipline =redis_cli.pipline()
        redis_pipline.setex('sms_'+mobile, constants.SMS_EXPIRES, sms_code)
        redis_pipline.setex('sms_flag_'+mobile, constants.SMS_FLAG_EXPIRES, 1)
        redis_pipline.excute()
        send_sms.delay(mobile, sms_code, constants.SMS_EXPIRES / 60, 1)
        return Response({"message": 'ok'})